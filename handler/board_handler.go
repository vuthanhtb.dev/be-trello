package handler

import (
	"be-trello/model"
	"be-trello/repository"
	"be-trello/services"
	"be-trello/storage"
	"be-trello/utils"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
)

func BoardRoute(router *gin.Engine, appContext utils.AppContext) {
	store := storage.NewSQLStore(appContext.GetMainDBConnection())
	repo := repository.NewBoardRepository(store)
	service := services.NewBoardService(repo)

	board := router.Group("/api/v1/boards")
	{
		board.POST("", createBoardHandler(service))
		board.GET("", listBoardHandler(service))
	}
}

func listBoardHandler(service services.BoardService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		data, err := service.ListBoardBusiness(ctx.Request.Context())

		if err != nil {
			ctx.JSON(http.StatusBadRequest, err.Error())
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"data": data})
	}
}

func createBoardHandler(service services.BoardService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var data model.Board

		if err := ctx.ShouldBind(&data); err != nil {
			ctx.JSON(http.StatusBadRequest, errors.New(err.Error()))
			return
		}

		if err := service.CreateBoardBusiness(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, err.Error())
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"message": "success"})
	}
}
