package handler

import (
	"be-trello/model"
	"be-trello/repository"
	"be-trello/services"
	"be-trello/storage"
	"be-trello/utils"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func CardRoute(router *gin.Engine, appContext utils.AppContext) {
	store := storage.NewSQLStore(appContext.GetMainDBConnection())
	repo := repository.NewCardRepository(store)
	service := services.NewCardService(repo)

	column := router.Group("/api/v1/cards")
	{
		column.GET("", listCardHandler(service))
		column.POST("", createCardHandler(service))
		column.GET("/:id", getCardByIdHandler(service))
		column.PUT("/:id", updateCardHandler(service))
		column.DELETE("/:id", deleteCardByIdHandler(service))

	}
}

func deleteCardByIdHandler(service services.CardService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		id, err := strconv.Atoi(ctx.Param("id"))

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		if err := service.DeleteCardService(ctx.Request.Context(), id); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"message": "success"})
	}
}

func updateCardHandler(service services.CardService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		id, err := strconv.Atoi(ctx.Param("id"))

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		var data model.Card

		if err := ctx.ShouldBind(&data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		if err := service.UpdateCardService(ctx.Request.Context(), id, &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"message": "success"})
	}
}

func getCardByIdHandler(service services.CardService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		id, err := strconv.Atoi(ctx.Param("id"))

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		data, err := service.GetCardByIdService(ctx.Request.Context(), id)

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"message": data})
	}
}

func listCardHandler(service services.CardService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		data, err := service.ListCardService(ctx.Request.Context())

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"data": data})
	}
}

func createCardHandler(service services.CardService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var data model.Card

		if err := ctx.ShouldBind(&data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		if err := service.CreateCardService(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"message": "success"})
	}
}
