package handler

import (
	"be-trello/model"
	"be-trello/repository"
	"be-trello/services"
	"be-trello/storage"
	"be-trello/utils"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func ColumnRoute(router *gin.Engine, appContext utils.AppContext) {
	store := storage.NewSQLStore(appContext.GetMainDBConnection())
	repo := repository.NewColumnRepository(store)
	service := services.NewColumnService(repo)

	column := router.Group("/api/v1/columns")
	{
		column.GET("", listColumnHandler(service))
		column.POST("", createColumnHandler(service))
		column.GET("/:id", getColumnByIdHandler(service))
		column.PUT("/:id", updateColumnHandler(service))
		column.DELETE("/:id", deleteColumnByIdHandler(service))

	}
}

func deleteColumnByIdHandler(service services.ColumnService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		id, err := strconv.Atoi(ctx.Param("id"))

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		if err := service.DeleteColumnService(ctx.Request.Context(), id); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"message": "success"})
	}
}

func updateColumnHandler(service services.ColumnService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		id, err := strconv.Atoi(ctx.Param("id"))

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		var data model.Column

		if err := ctx.ShouldBind(&data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		if err := service.UpdateColumnService(ctx.Request.Context(), id, &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"message": "success"})
	}
}

func getColumnByIdHandler(service services.ColumnService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		id, err := strconv.Atoi(ctx.Param("id"))

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		data, err := service.GetColumnByIdService(ctx.Request.Context(), id)

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"message": data})
	}
}

func listColumnHandler(service services.ColumnService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		data, err := service.ListColumnService(ctx.Request.Context())

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"data": data})
	}
}

func createColumnHandler(service services.ColumnService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var data model.Column

		if err := ctx.ShouldBind(&data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		if err := service.CreateColumnService(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"message": "success"})
	}
}
