package repository

import (
	"be-trello/model"
	"context"
	"errors"
)

type CardStore interface {
	InsertCardStore(_ context.Context, col *model.Card) error
	ListCardStore(_ context.Context, moreKeys ...string) ([]model.Card, error)
	GetCardByIdStore(_ context.Context, cond map[string]interface{}, moreKeys ...string) (*model.Card, error)
	UpdateCardStore(_ context.Context, data *model.Card, cond map[string]interface{}) error
	DeleteCardStore(_ context.Context, cond map[string]interface{}) error
}

type cardRepository struct {
	store CardStore
}

func NewCardRepository(store CardStore) *cardRepository {
	return &cardRepository{store: store}
}

func (repo *cardRepository) CreateCardRepo(ctx context.Context, col *model.Card) error {
	return repo.store.InsertCardStore(ctx, col)
}

func (repo *cardRepository) ListCardRepo(ctx context.Context) ([]model.Card, error) {
	return repo.store.ListCardStore(ctx)
}

func (repo *cardRepository) GetCardByIdRepo(ctx context.Context, id int) (*model.Card, error) {
	data, err := repo.store.GetCardByIdStore(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return nil, err
	}

	if data == nil {
		return nil, errors.New("not found")
	}

	return data, nil
}

func (repo *cardRepository) UpdateCardRepo(ctx context.Context, id int, data *model.Card) error {
	_, err := repo.store.GetCardByIdStore(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return errors.New("not found")
	}

	if err := repo.store.UpdateCardStore(ctx, data, map[string]interface{}{"id": id}); err != nil {
		return errors.New("update failed")
	}

	return nil
}

func (repo *cardRepository) DeleteCardRepo(ctx context.Context, id int) error {
	_, err := repo.store.GetCardByIdStore(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return errors.New("not found")
	}

	if err := repo.store.DeleteCardStore(ctx, map[string]interface{}{"id": id}); err != nil {
		return errors.New("delete failed")
	}

	return nil
}
