package repository

import (
	"be-trello/model"
	"context"
)

type BoardStore interface {
	ListBoardStore(ctx context.Context, moreKeys ...string) ([]model.Board, error)
	InsertBoardStore(ctx context.Context, board *model.Board) error
}

type boardRepository struct {
	store BoardStore
}

func NewBoardRepository(store BoardStore) *boardRepository {
	return &boardRepository{store: store}
}

func (repo *boardRepository) ListBoardRepo(ctx context.Context) ([]model.Board, error) {
	boards, err := repo.store.ListBoardStore(ctx)

	if err != nil {
		return nil, err
	}
	return boards, nil
}

func (repo *boardRepository) CreateBoardRepo(ctx context.Context, board *model.Board) error {
	return repo.store.InsertBoardStore(ctx, board)
}
