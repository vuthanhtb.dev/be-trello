package repository

import (
	"be-trello/model"
	"context"
	"errors"
)

type ColumnStore interface {
	InsertColumnStore(_ context.Context, col *model.Column) error
	ListColumnStore(_ context.Context, moreKeys ...string) ([]model.Column, error)
	GetColumnByIdStore(_ context.Context, cond map[string]interface{}, moreKeys ...string) (*model.Column, error)
	UpdateColumnStore(_ context.Context, data *model.Column, cond map[string]interface{}) error
	DeleteColumnStore(_ context.Context, cond map[string]interface{}) error
}

type columnRepository struct {
	store ColumnStore
}

func NewColumnRepository(store ColumnStore) *columnRepository {
	return &columnRepository{store: store}
}

func (repo *columnRepository) CreateColumnRepo(ctx context.Context, col *model.Column) error {
	return repo.store.InsertColumnStore(ctx, col)
}

func (repo *columnRepository) ListColumnRepo(ctx context.Context) ([]model.Column, error) {
	return repo.store.ListColumnStore(ctx)
}

func (repo *columnRepository) GetColumnByIdRepo(ctx context.Context, id int) (*model.Column, error) {
	data, err := repo.store.GetColumnByIdStore(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return nil, err
	}

	if data == nil {
		return nil, errors.New("not found")
	}

	return data, nil
}

func (repo *columnRepository) UpdateColumnRepo(ctx context.Context, id int, data *model.Column) error {
	_, err := repo.store.GetColumnByIdStore(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return errors.New("not found")
	}

	if err := repo.store.UpdateColumnStore(ctx, data, map[string]interface{}{"id": id}); err != nil {
		return errors.New("update failed")
	}

	return nil
}

func (repo *columnRepository) DeleteColumnRepo(ctx context.Context, id int) error {
	_, err := repo.store.GetColumnByIdStore(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return errors.New("not found")
	}

	if err := repo.store.DeleteColumnStore(ctx, map[string]interface{}{"id": id}); err != nil {
		return errors.New("delete failed")
	}

	return nil
}
