package services

import (
	"be-trello/model"
	"context"
)

type ColumnRepository interface {
	CreateColumnRepo(ctx context.Context, col *model.Column) error
	ListColumnRepo(ctx context.Context) ([]model.Column, error)
	GetColumnByIdRepo(ctx context.Context, id int) (*model.Column, error)
	UpdateColumnRepo(ctx context.Context, id int, data *model.Column) error
	DeleteColumnRepo(ctx context.Context, id int) error
}

type ColumnService interface {
	CreateColumnService(ctx context.Context, col *model.Column) error
	ListColumnService(ctx context.Context) ([]model.Column, error)
	GetColumnByIdService(ctx context.Context, id int) (*model.Column, error)
	UpdateColumnService(ctx context.Context, id int, data *model.Column) error
	DeleteColumnService(ctx context.Context, id int) error
}

type columnService struct {
	repo ColumnRepository
}

func NewColumnService(repo ColumnRepository) *columnService {
	return &columnService{repo: repo}
}

func (service *columnService) CreateColumnService(ctx context.Context, col *model.Column) error {
	return service.repo.CreateColumnRepo(ctx, col)
}

func (service *columnService) ListColumnService(ctx context.Context) ([]model.Column, error) {
	return service.repo.ListColumnRepo(ctx)
}

func (service *columnService) GetColumnByIdService(ctx context.Context, id int) (*model.Column, error) {
	return service.repo.GetColumnByIdRepo(ctx, id)
}

func (service *columnService) UpdateColumnService(ctx context.Context, id int, data *model.Column) error {
	return service.repo.UpdateColumnRepo(ctx, id, data)
}

func (service *columnService) DeleteColumnService(ctx context.Context, id int) error {
	return service.repo.DeleteColumnRepo(ctx, id)
}
