package services

import (
	"be-trello/model"
	"context"
)

type CardRepository interface {
	CreateCardRepo(ctx context.Context, col *model.Card) error
	ListCardRepo(ctx context.Context) ([]model.Card, error)
	GetCardByIdRepo(ctx context.Context, id int) (*model.Card, error)
	UpdateCardRepo(ctx context.Context, id int, data *model.Card) error
	DeleteCardRepo(ctx context.Context, id int) error
}

type CardService interface {
	CreateCardService(ctx context.Context, col *model.Card) error
	ListCardService(ctx context.Context) ([]model.Card, error)
	GetCardByIdService(ctx context.Context, id int) (*model.Card, error)
	UpdateCardService(ctx context.Context, id int, data *model.Card) error
	DeleteCardService(ctx context.Context, id int) error
}

type cardService struct {
	repo CardRepository
}

func NewCardService(repo CardRepository) *cardService {
	return &cardService{repo: repo}
}

func (service *cardService) CreateCardService(ctx context.Context, col *model.Card) error {
	return service.repo.CreateCardRepo(ctx, col)
}

func (service *cardService) ListCardService(ctx context.Context) ([]model.Card, error) {
	return service.repo.ListCardRepo(ctx)
}

func (service *cardService) GetCardByIdService(ctx context.Context, id int) (*model.Card, error) {
	return service.repo.GetCardByIdRepo(ctx, id)
}

func (service *cardService) UpdateCardService(ctx context.Context, id int, data *model.Card) error {
	return service.repo.UpdateCardRepo(ctx, id, data)
}

func (service *cardService) DeleteCardService(ctx context.Context, id int) error {
	return service.repo.DeleteCardRepo(ctx, id)
}
