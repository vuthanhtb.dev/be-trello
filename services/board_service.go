package services

import (
	"be-trello/model"
	"context"
)

type BoardRepository interface {
	ListBoardRepo(ctx context.Context) ([]model.Board, error)
	CreateBoardRepo(ctx context.Context, board *model.Board) error
}

type BoardService interface {
	ListBoardBusiness(ctx context.Context) ([]model.Board, error)
	CreateBoardBusiness(ctx context.Context, board *model.Board) error
}

type boardService struct {
	repo BoardRepository
}

func NewBoardService(repo BoardRepository) *boardService {
	return &boardService{repo: repo}
}

func (service *boardService) ListBoardBusiness(ctx context.Context) ([]model.Board, error) {
	return service.repo.ListBoardRepo(ctx)
}

func (service *boardService) CreateBoardBusiness(ctx context.Context, board *model.Board) error {
	return service.repo.CreateBoardRepo(ctx, board)
}
