package storage

import (
	"be-trello/model"
	"context"
	"errors"
	"gorm.io/gorm"
)

func (s *sqlStore) InsertColumnStore(_ context.Context, col *model.Column) error {
	col.PrepareForInsert()
	if err := s.db.Create(col).Error; err != nil {
		return errors.New("something went wrong with DB")
	}

	return nil
}

func (s *sqlStore) ListColumnStore(_ context.Context, _ ...string) ([]model.Column, error) {
	var result []model.Column

	if err := s.db.Table(model.Column{}.TableName()).Preload("Cards").Find(&result).Error; err != nil {
		return nil, errors.New("something went wrong with DB")
	}
	return result, nil
}

func (s *sqlStore) GetColumnByIdStore(_ context.Context, cond map[string]interface{}, _ ...string) (*model.Column, error) {
	var data model.Column

	if err := s.db.Where(cond).Preload("Cards").First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("not found")
		}
		return nil, errors.New("something went wrong with DB")
	}

	return &data, nil
}

func (s *sqlStore) UpdateColumnStore(_ context.Context, data *model.Column, cond map[string]interface{}) error {

	if err := s.db.Where(cond).Updates(data).Error; err != nil {
		return errors.New("something went wrong with DB")
	}

	return nil
}

func (s *sqlStore) DeleteColumnStore(_ context.Context, cond map[string]interface{}) error {
	if err := s.db.Table(model.Column{}.TableName()).Where(cond).Delete(nil).Error; err != nil {
		return errors.New("something went wrong with DB")
	}

	return nil
}
