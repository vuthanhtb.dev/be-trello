package storage

import (
	"be-trello/model"
	"context"
	"errors"
	"gorm.io/gorm"
)

func (s *sqlStore) InsertCardStore(_ context.Context, col *model.Card) error {
	col.PrepareForInsert()
	if err := s.db.Create(col).Error; err != nil {
		return errors.New("something went wrong with DB")
	}

	return nil
}

func (s *sqlStore) ListCardStore(_ context.Context, _ ...string) ([]model.Card, error) {
	var result []model.Card

	if err := s.db.Table(model.Card{}.TableName()).Find(&result).Error; err != nil {
		return nil, errors.New("something went wrong with DB")
	}
	return result, nil
}

func (s *sqlStore) GetCardByIdStore(_ context.Context, cond map[string]interface{}, _ ...string) (*model.Card, error) {
	var data model.Card

	if err := s.db.Where(cond).First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("not found")
		}
		return nil, errors.New("something went wrong with DB")
	}

	return &data, nil
}

func (s *sqlStore) UpdateCardStore(_ context.Context, data *model.Card, cond map[string]interface{}) error {

	if err := s.db.Where(cond).Updates(data).Error; err != nil {
		return errors.New("something went wrong with DB")
	}

	return nil
}

func (s *sqlStore) DeleteCardStore(_ context.Context, cond map[string]interface{}) error {
	if err := s.db.Table(model.Card{}.TableName()).Where(cond).Delete(nil).Error; err != nil {
		return errors.New("something went wrong with DB")
	}

	return nil
}
