package storage

import (
	"be-trello/model"
	"context"
	"errors"
)

func (s *sqlStore) InsertBoardStore(_ context.Context, board *model.Board) error {
	board.PrepareForInsert()
	if err := s.db.Create(board).Error; err != nil {
		return errors.New("something went wrong with DB")
	}

	return nil
}

func (s *sqlStore) ListBoardStore(_ context.Context, _ ...string) ([]model.Board, error) {
	var result []model.Board

	if err := s.db.Table(model.Board{}.TableName()).Preload("Columns.Cards").Find(&result).Error; err != nil {
		return nil, errors.New("something went wrong with DB")
	}
	return result, nil
}
