package main

import (
	"be-trello/handler"
	"be-trello/model"
	"be-trello/utils"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"os"
)

func main() {
	dsn := os.Getenv("DB_CONN_STR")
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatalln("Cannot connect to MySQL:", err)
	}

	if err := db.AutoMigrate(&model.Board{}, &model.Card{}, &model.Column{}); err != nil {
		log.Fatalln(err.Error())
	}

	router := gin.Default()

	appContext := utils.NewAppContext(db)
	handler.BoardRoute(router, appContext)
	handler.ColumnRoute(router, appContext)
	handler.CardRoute(router, appContext)

	if err := router.Run(":8081"); err != nil {
		log.Fatalln(err)
	}
}
