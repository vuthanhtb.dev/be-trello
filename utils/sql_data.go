package utils

import "time"

type SQLModel struct {
	Id        int        `json:"id" gorm:"column:id"`
	Title     string     `json:"title" gorm:"column:title"`
	Destroy   bool       `json:"destroy" gorm:"column:destroy"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at;"`
	UpdateAt  *time.Time `json:"updated_at" gorm:"column:updated_at;"`
}

func (sqlModel *SQLModel) PrepareForInsert() {
	now := time.Now().UTC()
	sqlModel.Id = 0
	sqlModel.Destroy = false
	sqlModel.CreatedAt = &now
	sqlModel.UpdateAt = &now
}
