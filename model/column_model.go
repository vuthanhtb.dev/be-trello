package model

import "be-trello/utils"

type Column struct {
	utils.SQLModel `json:",inline"`
	BoardId        int    `json:"board_id" gorm:"column:board_id"`
	CardOrder      string `json:"card_order" gorm:"column:card_order"`
	Cards          []Card `json:"cards" gorm:"foreignKey:ColumnId;PRELOAD:false;"`
}

func (Column) TableName() string {
	return "columns"
}
