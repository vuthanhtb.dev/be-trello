package model

import "be-trello/utils"

type Board struct {
	utils.SQLModel `json:",inline"`
	ColumnOrder    string   `json:"column_order" gorm:"column:column_order"`
	Columns        []Column `json:"columns" gorm:"foreignKey:BoardId;PRELOAD:false;""`
}

func (Board) TableName() string {
	return "boards"
}
