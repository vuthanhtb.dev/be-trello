package model

import "be-trello/utils"

type Card struct {
	utils.SQLModel `json:",inline"`
	Cover          string `json:"cover" gorm:"column:cover"`
	BoardId        int    `json:"board_id" gorm:"column:board_id"`
	ColumnId       int    `json:"column_id" gorm:"column:column_id"`
}

func (Card) TableName() string {
	return "cards"
}
